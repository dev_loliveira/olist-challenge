
from .common import *


# Unittest configuration variables
TEST_RUNNER = 'django_nose.NoseTestSuiteRunner'
NOSE_ARGS = (
    '--with-coverage',
    '-s',                  # Avoid the capture of stdout so we can use the python debugger
    '--cover-package=api',
)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': ':memory:',
    }
}