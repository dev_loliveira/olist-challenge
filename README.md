# Olist Challenge
This project has been created as a requirement for the position of backend developer at [Olist](https://olist.com/)


## About the project
This challenge project was created with the purpose of calculating monthly telephone bills of a phone number in a given closed period. It achieves this goal by using a REST API approach. In total, there are two mapped urls that this application answer for:
- submission of telephone records
- retrieval of monthly bills

## API Documentation and Cloud environment
- [Documentation](https://documenter.getpostman.com/view/2929338/S1LpaWxv)
- [http://olist-devloliveira.herokuapp.com](http://olist-devloliveira.herokuapp.com)

# Installation

## Creating the virtual environment
```
virtualenv --python /usr/bin/python3.5 .venv
source .venv/bin/activate
```

## Installing the dependencies
```
pip install -r requirements.txt
```

## Running the migrations
```
python manage.py migrate
```

## Running the unittests
```
python manage.py test
```

## Running the server locally
```
python manage.py runserver
```

### Coding environment

The following table elaborates the setup used to code this project.

| Type | Technology |
| ------ | ------ |
| OS | Linux - Debian |
| IDE | Sublime Text (with PEP8 plugin) |
| Cloud host | Heroku |
| library | Django |
| library | pytz |
| library | djangorestframework |
| library | coverage |
| library | mock |
| library | model_mommy |
| library | ipython |
| library | money |
| library | babel |
