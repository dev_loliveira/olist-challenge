# Generated by Django 2.0.3 on 2019-04-25 12:28

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='PhoneRecord',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('call_identifier', models.IntegerField()),
                ('record_type', models.CharField(default='', max_length=10)),
                ('record_start', models.DateTimeField()),
                ('record_end', models.DateTimeField(blank=True, null=True)),
                ('origin_number', models.CharField(max_length=12)),
                ('destination_number', models.CharField(max_length=12)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
