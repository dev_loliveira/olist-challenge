
import pytz
from datetime import date, datetime, timedelta
from django.test import TestCase
from api.models import PhoneRecord


class PhoneRecordPeriodBillMixinTest(TestCase):

    def test_is_closed_period_should_return_False_if_provided_period_month_is_greater_than_last_month(self):
        date_a = datetime.today()
        date_b = date_a + timedelta(days=30)

        self.assertFalse(PhoneRecord.is_closed_period(date_a))
        self.assertFalse(PhoneRecord.is_closed_period(date_b))

    def test_is_closed_period_should_return_True_for_testing_input(self):
        date_a = date(year=2017, month=12, day=11)

        self.assertTrue(PhoneRecord.is_closed_period(date_a))

    def test_is_closed_period_should_return_True_if_provided_period_month_is_lower_than_current_month(self):
        date_a = datetime.today() - timedelta(days=30)
        date_b = date_a - timedelta(days=30)

        self.assertTrue(PhoneRecord.is_closed_period(date_a))
        self.assertTrue(PhoneRecord.is_closed_period(date_b))

    def test_get_closed_period_should_return_the_first_day_of_the_previous_month(self):
        day_first = datetime.today().replace(day=1)

        expected = (day_first - timedelta(days=1)).replace(day=1)
        returned = PhoneRecord.get_last_closed_period()
        self.assertEqual(1, returned.day)
        self.assertEqual(expected.month, returned.month)
        self.assertEqual(expected.year, returned.year)

    def test_get_period_bill_should_return_an_empty_call_record_if_period_has_no_available_records(self):
        closed_period = PhoneRecord.get_last_closed_period()

        expected_origin_number = '21988526423'
        expected_reference_period = closed_period.strftime('%m/%Y')
        expected_bill_list = []
        returned = PhoneRecord.get_period_bill('21988526423', closed_period)

        self.assertIn('origin_number', returned)
        self.assertIn('reference_period', returned)
        self.assertIn('bill_list', returned)
        self.assertEqual(expected_origin_number, returned['origin_number'])
        self.assertEqual(expected_reference_period, returned['reference_period'])
        self.assertEqual(expected_bill_list, returned['bill_list'])

    def test_get_period_bill_should_return_the_correct_number_of_bills_available_in_the_specified_period(self):
        reference_period = date(year=2016, month=2, day=1)

        PhoneRecord.objects.create(
            call_identifier='123',
            record_start=datetime(year=2016, month=2, day=1,
                              hour=12, minute=0, second=0),
            record_end=datetime(year=2016, month=2, day=1,
                            hour=12, minute=0, second=1),
            origin_number='21988526423',
            destination_number='2111111111',
        )

        PhoneRecord.objects.create(
            call_identifier='123',
            record_start=datetime(year=2016, month=2, day=2,
                              hour=12, minute=0, second=0),
            record_end=datetime(year=2016, month=2, day=2,
                            hour=12, minute=0, second=1),
            origin_number='21988526423',
            destination_number='2111111112',
        )

        PhoneRecord.objects.create(
            call_identifier='123',
            record_start=datetime(year=2016, month=2, day=3,
                              hour=12, minute=0, second=0),
            record_end=datetime(year=2016, month=2, day=3,
                            hour=12, minute=0, second=1),
            origin_number='21988526423',
            destination_number='2111111113',
        )

        # This phone record should be ignore as it belongs to a different period
        PhoneRecord.objects.create(
            call_identifier='123',
            record_start=datetime(year=2016, month=3, day=1,
                              hour=12, minute=0, second=0),
            record_end=datetime(year=2016, month=3, day=1,
                            hour=12, minute=0, second=1),
            origin_number='21988526423',
            destination_number='2111111113',
        )

        expected_origin_number = '21988526423'
        expected_reference_period = reference_period.strftime('%m/%Y')
        returned = PhoneRecord.get_period_bill('21988526423', reference_period)

        self.assertIn('origin_number', returned)
        self.assertIn('reference_period', returned)
        self.assertIn('bill_list', returned)
        self.assertEqual(expected_origin_number, returned['origin_number'])
        self.assertEqual(expected_reference_period, returned['reference_period'])
        self.assertEqual(3, len(returned['bill_list']))

    def test_get_period_bill_should_return_the_correct_data_format_of_bills_available_in_the_specified_period(self):
        reference_period = date(year=2016, month=3, day=1)

        PhoneRecord.objects.create(
            call_identifier='123',
            record_start=datetime(year=2016, month=3, day=1,
                              hour=12, minute=0, second=0),
            record_end=datetime(year=2016, month=3, day=1,
                            hour=12, minute=0, second=1),
            origin_number='21988526423',
            destination_number='2111111111',
        )

        PhoneRecord.objects.create(
            call_identifier='123',
            record_start=datetime(year=2016, month=3, day=2,
                              hour=13, minute=0, second=0),
            record_end=datetime(year=2016, month=3, day=2,
                            hour=13, minute=4, second=1),
            origin_number='21988526423',
            destination_number='2111111112',
        )

        PhoneRecord.objects.create(
            call_identifier='123',
            record_start=datetime(year=2016, month=3, day=3,
                              hour=12, minute=50, second=0),
            record_end=datetime(year=2016, month=3, day=3,
                            hour=13, minute=0, second=1),
            origin_number='21988526423',
            destination_number='2111111113',
        )

        PhoneRecord.objects.create(
            call_identifier='123',
            record_start=datetime(year=2016, month=3, day=15,
                              hour=17, minute=0, second=0),
            record_end=datetime(year=2016, month=3, day=15,
                            hour=18, minute=50, second=59),
            origin_number='21988526423',
            destination_number='2111111113',
        )

        expected_origin_number = '21988526423'
        expected_reference_period = reference_period.strftime('%m/%Y')
        expected_first_bill = {
            'destination_number': '2111111111',
            'call_start_date': '03/01/2016',
            'call_start_time': '12:00:00',
            'call_duration': '0h0m1s',
            'call_price': 'R$ 0,36'
        }
        expected_second_bill = {
            'destination_number': '2111111112',
            'call_start_date': '03/02/2016',
            'call_start_time': '13:00:00',
            'call_duration': '0h4m1s',
            'call_price': 'R$ 0,72'
        }
        expected_third_bill = {
            'destination_number': '2111111113',
            'call_start_date': '03/03/2016',
            'call_start_time': '12:50:00',
            'call_duration': '0h10m1s',
            'call_price': 'R$ 1,26'
        }
        expected_fourth_bill = {
            'destination_number': '2111111113',
            'call_start_date': '03/15/2016',
            'call_start_time': '17:00:00',
            'call_duration': '1h50m59s',
            'call_price': 'R$ 10,26'
        }
        returned = PhoneRecord.get_period_bill('21988526423', reference_period)

        self.assertIn('origin_number', returned)
        self.assertIn('reference_period', returned)
        self.assertIn('bill_list', returned)
        self.assertEqual(expected_origin_number, returned['origin_number'])
        self.assertEqual(expected_reference_period, returned['reference_period'])
        self.assertEqual(expected_first_bill, returned['bill_list'][0])
        self.assertEqual(expected_second_bill, returned['bill_list'][1])
        self.assertEqual(expected_third_bill, returned['bill_list'][2])
        self.assertEqual(expected_fourth_bill, returned['bill_list'][3])

    def test_get_period_bill_should_return_the_correct_data_format_of_bills_available_in_the_last_closed_period(self):
        reference_period = PhoneRecord.get_last_closed_period()

        PhoneRecord.objects.create(
            call_identifier='123',
            record_start=datetime(year=reference_period.year, month=reference_period.month, day=1,
                              hour=12, minute=0, second=0),
            record_end=datetime(year=reference_period.year, month=reference_period.month, day=1,
                            hour=12, minute=0, second=1),
            origin_number='21988526423',
            destination_number='2111111111',
        )

        PhoneRecord.objects.create(
            call_identifier='123',
            record_start=datetime(year=reference_period.year, month=reference_period.month, day=2,
                              hour=13, minute=0, second=0),
            record_end=datetime(year=reference_period.year, month=reference_period.month, day=2,
                            hour=13, minute=4, second=1),
            origin_number='21988526423',
            destination_number='2111111112',
        )

        PhoneRecord.objects.create(
            call_identifier='123',
            record_start=datetime(year=reference_period.year, month=reference_period.month, day=3,
                              hour=12, minute=50, second=0),
            record_end=datetime(year=reference_period.year, month=reference_period.month, day=3,
                            hour=13, minute=0, second=1),
            origin_number='21988526423',
            destination_number='2111111113',
        )

        PhoneRecord.objects.create(
            call_identifier='123',
            record_start=datetime(year=reference_period.year, month=reference_period.month, day=4,
                              hour=17, minute=0, second=0),
            record_end=datetime(year=reference_period.year, month=reference_period.month, day=4,
                            hour=18, minute=50, second=59),
            origin_number='21988526423',
            destination_number='2111111113',
        )

        expected_origin_number = '21988526423'
        expected_reference_period = reference_period.strftime('%m/%Y')
        expected_first_bill = {
            'destination_number': '2111111111',
            'call_start_date': reference_period.strftime('%m/01/%Y'),
            'call_start_time': '12:00:00',
            'call_duration': '0h0m1s',
            'call_price': 'R$ 0,36'
        }
        expected_second_bill = {
            'destination_number': '2111111112',
            'call_start_date': reference_period.strftime('%m/02/%Y'),
            'call_start_time': '13:00:00',
            'call_duration': '0h4m1s',
            'call_price': 'R$ 0,72'
        }
        expected_third_bill = {
            'destination_number': '2111111113',
            'call_start_date': reference_period.strftime('%m/03/%Y'),
            'call_start_time': '12:50:00',
            'call_duration': '0h10m1s',
            'call_price': 'R$ 1,26'
        }
        expected_fourth_bill = {
            'destination_number': '2111111113',
            'call_start_date': reference_period.strftime('%m/04/%Y'),
            'call_start_time': '17:00:00',
            'call_duration': '1h50m59s',
            'call_price': 'R$ 10,26'
        }
        returned = PhoneRecord.get_period_bill('21988526423')

        self.assertIn('origin_number', returned)
        self.assertIn('reference_period', returned)
        self.assertIn('bill_list', returned)
        self.assertEqual(expected_origin_number, returned['origin_number'])
        self.assertEqual(expected_reference_period, returned['reference_period'])
        self.assertEqual(expected_first_bill, returned['bill_list'][0])
        self.assertEqual(expected_second_bill, returned['bill_list'][1])
        self.assertEqual(expected_third_bill, returned['bill_list'][2])
        self.assertEqual(expected_fourth_bill, returned['bill_list'][3])


    def test_get_period_bill_should_not_process_incomplete_records(self):
        reference_period = PhoneRecord.get_last_closed_period()

        PhoneRecord.objects.create(
            call_identifier='123',
            origin_number='21988526423',
            destination_number='2111111111',
        )


        PhoneRecord.objects.create(
            call_identifier='123',
            record_end=datetime(year=reference_period.year, month=reference_period.month, day=4,
                            hour=18, minute=50, second=59),
            origin_number='21988526423',
            destination_number='2111111113',
        )

        PhoneRecord.objects.create(
            call_identifier='123',
            record_start=datetime(year=reference_period.year, month=reference_period.month, day=4,
                              hour=17, minute=0, second=0),
            origin_number='21988526423',
            destination_number='2111111113',
        )


        PhoneRecord.objects.create(
            call_identifier='123',
            record_start=datetime(year=reference_period.year, month=reference_period.month, day=4,
                              hour=17, minute=0, second=0),
            record_end=datetime(year=reference_period.year, month=reference_period.month, day=4,
                            hour=18, minute=50, second=59),
            origin_number='21988526423',
            destination_number='2111111113',
        )

        expected_origin_number = '21988526423'
        expected_reference_period = reference_period.strftime('%m/%Y')
        expected_fourth_bill = {
            'destination_number': '2111111113',
            'call_start_date': reference_period.strftime('%m/04/%Y'),
            'call_start_time': '17:00:00',
            'call_duration': '1h50m59s',
            'call_price': 'R$ 10,26'
        }
        returned = PhoneRecord.get_period_bill('21988526423')

        self.assertIn('origin_number', returned)
        self.assertIn('reference_period', returned)
        self.assertIn('bill_list', returned)
        self.assertEqual(expected_origin_number, returned['origin_number'])
        self.assertEqual(expected_reference_period, returned['reference_period'])
        self.assertEqual(expected_fourth_bill, returned['bill_list'][0])
