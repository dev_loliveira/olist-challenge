
import pytz
from datetime import datetime
from django.test import TestCase
from api.models import PhoneRecord


class PhoneRecordGetObjectFromJsonTest(TestCase):

    def test_should_create_a_new_record_if_type_is_START(self):
        expected_start = datetime(
            year=2016, month=2, day=29, hour=10, minute=11, second=12, tzinfo=pytz.UTC)
        returned = PhoneRecord.get_object_from_json({
            "id":  1,
            "call_id":  "123",
            "type":  "start",
            "timestamp":  "2016-02-29T10:11:12Z",
            "source":  "21988526423",
            "destination":  "2133468278"
        })

        created_record = PhoneRecord.objects.filter(call_identifier='123')
        self.assertEqual(1, len(created_record))
        self.assertEqual(expected_start, created_record[0].record_start)
        self.assertEqual(None, created_record[0].record_end)
        self.assertEqual('21988526423', created_record[0].origin_number)
        self.assertEqual('2133468278', created_record[0].destination_number)

    def test_should_type_start_should_be_case_insensitive(self):
        expected_start = datetime(
            year=2016, month=2, day=29, hour=10, minute=11, second=12, tzinfo=pytz.UTC)
        returned = PhoneRecord.get_object_from_json({
            "id":  1,
            "call_id":  "123",
            "type":  "START",
            "timestamp":  "2016-02-29T10:11:12Z",
            "source":  "21988526423",
            "destination":  "2133468278"
        })

        created_record = PhoneRecord.objects.filter(call_identifier='123')
        self.assertEqual(1, len(created_record))
        self.assertEqual(expected_start, created_record[0].record_start)
        self.assertEqual(None, created_record[0].record_end)
        self.assertEqual('21988526423', created_record[0].origin_number)
        self.assertEqual('2133468278', created_record[0].destination_number)

    def test_should_type_end_should_be_case_insensitive(self):
        expected_start = datetime(
            year=2016, month=2, day=29, hour=10, minute=11, second=12, tzinfo=pytz.UTC)
        expected_end = datetime(
            year=2016, month=2, day=29, hour=10, minute=15, second=0, tzinfo=pytz.UTC)
        PhoneRecord.objects.create(
            call_identifier='123',
            record_start=expected_start,
            origin_number='21988526423',
            destination_number='2133468278',
        )
        returned = PhoneRecord.get_object_from_json({
            "id":  1,
            "call_id":  "123",
            "type":  "END",
            "timestamp":  "2016-02-29T10:15:00Z",
        })

        record = PhoneRecord.objects.filter(call_identifier='123')
        self.assertEqual(1, len(record))
        self.assertEqual(expected_start, record[0].record_start)
        self.assertEqual(expected_end, record[0].record_end)
        self.assertEqual('21988526423', record[0].origin_number)
        self.assertEqual('2133468278', record[0].destination_number)

    def test_should_update_record_if_type_is_END(self):
        expected_start = datetime(
            year=2016, month=2, day=29, hour=10, minute=11, second=12, tzinfo=pytz.UTC)
        expected_end = datetime(
            year=2016, month=2, day=29, hour=10, minute=15, second=0, tzinfo=pytz.UTC)
        PhoneRecord.objects.create(
            call_identifier='123',
            record_start=expected_start,
            origin_number='21988526423',
            destination_number='2133468278',
        )
        returned = PhoneRecord.get_object_from_json({
            "id":  1,
            "call_id":  "123",
            "type":  "end",
            "timestamp":  "2016-02-29T10:15:00Z",
        })

        record = PhoneRecord.objects.filter(call_identifier='123')
        self.assertEqual(1, len(record))
        self.assertEqual(expected_start, record[0].record_start)
        self.assertEqual(expected_end, record[0].record_end)
        self.assertEqual('21988526423', record[0].origin_number)
        self.assertEqual('2133468278', record[0].destination_number)

    def test_should_update_existing_record_if_type_is_START(self):
        record_start = datetime(
            year=2016, month=2, day=29, hour=10, minute=11, second=12, tzinfo=pytz.UTC)
        record_end = datetime(
            year=2016, month=2, day=29, hour=11, minute=11, second=12, tzinfo=pytz.UTC)

        PhoneRecord.objects.create(call_identifier='123', record_end=record_end)

        returned = PhoneRecord.get_object_from_json({
            "id":  1,
            "call_id":  "123",
            "type":  "start",
            "timestamp":  "2016-02-29T10:11:12Z",
            "source":  "21988526423",
            "destination":  "2133468278"
        })

        updated_record = PhoneRecord.objects.filter(call_identifier='123')
        self.assertEqual(1, len(updated_record))
        self.assertEqual(record_start, updated_record[0].record_start)
        self.assertEqual(record_end, updated_record[0].record_end)
        self.assertEqual('21988526423', updated_record[0].origin_number)
        self.assertEqual('2133468278', updated_record[0].destination_number)

    def test_should_create_new_record_if_type_is_END_and_record_does_not_exist(self):
        record_end = datetime(
            year=2016, month=2, day=29, hour=11, minute=11, second=12, tzinfo=pytz.UTC)

        returned = PhoneRecord.get_object_from_json({
            "id":  1,
            "call_id":  "123",
            "type":  "end",
            "timestamp":  "2016-02-29T11:11:12Z",
        })

        created_record = PhoneRecord.objects.filter(call_identifier='123')
        self.assertEqual(1, len(created_record))
        self.assertEqual(None, created_record[0].record_start)
        self.assertEqual(record_end, created_record[0].record_end)
        self.assertEqual('', created_record[0].origin_number)
        self.assertEqual('', created_record[0].destination_number)
