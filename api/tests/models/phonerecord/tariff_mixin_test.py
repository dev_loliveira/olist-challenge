
import pytz
from datetime import datetime
from django.test import TestCase
from api.models import PhoneRecord
from api.mixins import PhoneRecordTariffCalculatorMixin


class PhoneRecordTariffCalculatorMixinTest(TestCase):

    def test_should_apply_only_the_base_tariff_if_call_duration_is_lesser_than_a_minute_during_standard_period(self):
        call_start = datetime(year=2016, month=2, day=29,
                              hour=12, minute=0, second=0)

        call_end = datetime(year=2016, month=2, day=29,
                            hour=12, minute=0, second=1)
        PhoneRecord.objects.create(
            call_identifier='123',
            record_start=call_start,
            record_end=call_end,
            origin_number='2133468278',
            destination_number='21988526423',
        )

        record = PhoneRecord.objects.get(call_identifier='123')
        expected = 0.36
        returned = record.tariff
        self.assertEqual(expected, returned)

    def test_should_apply_only_the_base_tariff_if_call_duration_is_lesser_than_a_minute_during_reduced_period(self):
        call_start = datetime(year=2016, month=2, day=29,
                              hour=23, minute=0, second=0)

        call_end = datetime(year=2016, month=2, day=29,
                            hour=0, minute=0, second=1)
        PhoneRecord.objects.create(
            call_identifier='123',
            record_start=call_start,
            record_end=call_end,
            origin_number='2133468278',
            destination_number='21988526423',
        )

        record = PhoneRecord.objects.get(call_identifier='123')
        expected = 0.36
        returned = record.tariff
        self.assertEqual(expected, returned)

    def test_should_correctly_calculate_the_tariff_when_call_occurs_during_standard_period(self):
        call_start = datetime(year=2016, month=2, day=29,
                              hour=6, minute=0, second=0)

        call_end = datetime(year=2016, month=2, day=29,
                            hour=7, minute=0, second=59)
        PhoneRecord.objects.create(
            call_identifier='123',
            record_start=call_start,
            record_end=call_end,
            origin_number='2133468278',
            destination_number='21988526423',
        )

        record = PhoneRecord.objects.get(call_identifier='123')
        base_tariff = 0.36
        call_charge = 5.4              # Call with one hour of duration
        expected = float('%.2f' % (call_charge + base_tariff))
        returned = record.tariff
        self.assertEqual(expected, returned)

    def test_should_not_charge_during_reduced_tariff_period(self):
        call_start = datetime(year=2016, month=1, day=1,
                              hour=21, minute=0, second=0)

        call_end = datetime(year=2016, month=1, day=2,
                            hour=6, minute=0, second=0)
        PhoneRecord.objects.create(
            call_identifier='123',
            record_start=call_start,
            record_end=call_end,
            origin_number='2133468278',
            destination_number='21988526423',
        )

        record = PhoneRecord.objects.get(call_identifier='123')
        base_tariff = 0.36
        call_charge = 5.4              # Call with one hour of duration on standard period
        expected = float('%.2f' % (call_charge + base_tariff))
        returned = record.tariff
        self.assertEqual(expected, returned)

    def test_example_from_repository_should_return_the_expected_result(self):
        call_start = datetime(year=2016, month=1, day=1,
                              hour=21, minute=57, second=13)

        call_end = datetime(year=2016, month=1, day=1,
                            hour=22, minute=17, second=53)
        PhoneRecord.objects.create(
            call_identifier='123',
            record_start=call_start,
            record_end=call_end,
            origin_number='2133468278',
            destination_number='21988526423',
        )

        record = PhoneRecord.objects.get(call_identifier='123')
        expected = .54
        returned = record.tariff
        self.assertEqual(expected, returned)
