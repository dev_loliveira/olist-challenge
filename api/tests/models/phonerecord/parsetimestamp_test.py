
from datetime import datetime
from django.test import TestCase
from api.models import PhoneRecord


class PhoneRecordTimestampTest(TestCase):

    def test_parse_timestamp_should_return_the_expected_datetime_object(self):
        expected_A = datetime(year=2016, month=2, day=29,
                              hour=12, minute=0, second=0)
        returned_A = PhoneRecord.parse_timestamp('2016-02-29T12:00:00Z')

        expected_B = datetime(year=2017, month=12, day=11,
                              hour=15, minute=7, second=13)
        returned_B = PhoneRecord.parse_timestamp('2017-12-11T15:07:13Z')

        expected_C = datetime(year=2017, month=12, day=12,
                              hour=22, minute=47, second=56)
        returned_C = PhoneRecord.parse_timestamp('2017-12-12T22:47:56Z')

        expected_D = datetime(year=2017, month=12, day=12,
                              hour=21, minute=57, second=13)
        returned_D = PhoneRecord.parse_timestamp('2017-12-12T21:57:13Z')

        expected_E = datetime(year=2017, month=12, day=12,
                              hour=4, minute=57, second=13)
        returned_E = PhoneRecord.parse_timestamp('2017-12-12T04:57:13Z')

        expected_F = datetime(year=2017, month=12, day=13,
                              hour=21, minute=57, second=13)
        returned_F = PhoneRecord.parse_timestamp('2017-12-13T21:57:13Z')

        expected_G = datetime(year=2017, month=12, day=12,
                              hour=15, minute=7, second=58)
        returned_G = PhoneRecord.parse_timestamp('2017-12-12T15:07:58Z')

        expected_H = datetime(year=2018, month=2, day=28,
                              hour=21, minute=57, second=13)
        returned_H = PhoneRecord.parse_timestamp('2018-02-28T21:57:13Z')

        self.assertEqual(expected_A, returned_A)
        self.assertEqual(expected_B, returned_B)
        self.assertEqual(expected_C, returned_C)
        self.assertEqual(expected_D, returned_D)
        self.assertEqual(expected_E, returned_E)
        self.assertEqual(expected_F, returned_F)
        self.assertEqual(expected_G, returned_G)
        self.assertEqual(expected_H, returned_H)

    def test_parse_timestamp_should_be_able_to_parse_ISO8601_format(self):
        expected_A = datetime(year=2011, month=1, day=15,
                              hour=9, minute=12, second=1)
        returned_A = PhoneRecord.parse_timestamp('2011-01-15T09:12:01.243860Z')

        self.assertEqual(expected_A, returned_A)

    def test_parse_timestamp_should_assume_an_UTC_timestamp(self):
        expected_A = datetime(year=2011, month=1, day=15,
                              hour=9, minute=12, second=1)
        returned_A = PhoneRecord.parse_timestamp('2011-01-15T09:12:01.243860')

        self.assertEqual(expected_A, returned_A)

    def test_parse_timestamp_should_return_None_if_timestamp_format_is_invalid(self):
        expected = None
        returned = PhoneRecord.parse_timestamp('2017-01-01')

        self.assertEqual(expected, returned)
