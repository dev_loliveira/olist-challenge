import time
from money import Money
from datetime import datetime, timedelta
from django.db import models
from django.db.models import CASCADE, SET_NULL
from api.mixins import (
    PhoneRecordTariffCalculatorMixin,
    PhoneRecordPeriodBillMixin
)


class BaseModel(models.Model):
    id = models.AutoField(primary_key=True)

    class Meta:
        abstract = True


class PhoneRecord(BaseModel, PhoneRecordPeriodBillMixin, PhoneRecordTariffCalculatorMixin):
    TYPE_START = 'start'
    TYPE_END = 'end'

    call_identifier = models.IntegerField()
    record_type = models.CharField(max_length=10, default='')
    record_start = models.DateTimeField(blank=True, null=True)
    record_end = models.DateTimeField(blank=True, null=True)
    origin_number = models.CharField(max_length=12)
    destination_number = models.CharField(max_length=12)

    @staticmethod
    def parse_timestamp(timestamp):
        available_formats = (
            '%Y-%m-%dT%H:%M:%SZ',
            '%Y-%m-%dT%H:%M:%S.%fZ',
        )

        # We assume an UTC timestamp
        timestamp = timestamp if timestamp.endswith('Z') else '%sZ' % timestamp

        attempts = list()

        for _format in available_formats:
            try:
                datetime_obj = datetime.strptime(timestamp, _format)
                return datetime_obj.replace(microsecond=0)
            except ValueError:
                continue

        return None

    @staticmethod
    def get_object_from_json(json_dict):
        record_type = json_dict['type'].lower()
        call_id = json_dict['call_id']

        if record_type == PhoneRecord.TYPE_START:
            record = PhoneRecord.objects.filter(call_identifier=call_id)

            if not record:
                data = {
                    'call_identifier': call_id,
                    'origin_number': json_dict['source'],
                    'destination_number': json_dict['destination'],
                    'record_start': PhoneRecord.parse_timestamp(json_dict['timestamp'])
                }
                return PhoneRecord.objects.create(**data)
            else:
                record = record.first()
                record.origin_number = json_dict['source']
                record.destination_number = json_dict['destination']
                record.record_start = PhoneRecord.parse_timestamp(json_dict['timestamp'])
                record.save()
                return record

        elif record_type == PhoneRecord.TYPE_END:
            record = PhoneRecord.objects.filter(
                call_identifier=json_dict['call_id'])

            if record:
                record = record.first()
            else:
                record = PhoneRecord(
                    call_identifier=call_id)

            record.record_end = PhoneRecord.parse_timestamp(
                json_dict['timestamp'])
            record.save()
            return record

        else:
            raise Exception('Invalid record type: %s' % (json_dict['type']))

    @staticmethod
    def get_period_bill(origin_number, reference_period=None):
        data = {
            'origin_number': origin_number,
            'bill_list': list()
        }

        if not reference_period:
            reference_period = PhoneRecordPeriodBillMixin.get_last_closed_period()

        if PhoneRecordPeriodBillMixin.is_closed_period(reference_period):

            bills = list()
            records = PhoneRecord.objects \
                .filter(origin_number=origin_number) \
                .filter(record_start__isnull=False) \
                .filter(record_end__year=reference_period.year) \
                .filter(record_end__month=reference_period.month) \
                .order_by('record_start')

            for record in records:
                time_diff = record.record_end - record.record_start
                duration = time_diff.total_seconds()

                # @source: https://stackoverflow.com/a/4048773
                timedelta_seconds = timedelta(seconds=duration)
                date_obj = datetime(1, 1, 1) + timedelta_seconds
                duration_format = '{hour}h{minute}m{second}s'.format(
                    hour=date_obj.hour + ((date_obj.day-1) * 24),
                    minute=date_obj.minute,
                    second=date_obj.second)

                bills.append({
                    'destination_number': record.destination_number,
                    'call_start_date': record.record_start.strftime('%m/%d/%Y'),
                    'call_start_time': record.record_start.strftime('%H:%M:%S'),
                    'call_duration': duration_format,
                    'call_price': Money(record.tariff, currency='BRL').format('pt_BR', 'R$ #,##0.00')
                })

            data['bill_list'] = bills

        data.update({
            'reference_period': reference_period.strftime('%m/%Y')
        })

        return data
