from django.conf.urls import include, url

from . import views

app_name = 'v1'

urlpatterns = [
    url(r'^phone/record/', include([
        url(r'^$', views.PhoneRecordAPIView.as_view(), name='record'),
        url(r'bills/(?P<origin_number>[0-9]+)/$', views.PhoneRecordBillsAPIView.as_view(), name='bills'),
        url(r'bills/(?P<origin_number>[0-9]+)/(?P<reference_period>[\w\W]+)/$', views.PhoneRecordBillsAPIView.as_view(), name='bills-from-period'),
    ])),
]
