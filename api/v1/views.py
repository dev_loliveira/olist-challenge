
from datetime import date
from rest_framework.views import APIView, Response
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from api.models import PhoneRecord


class PhoneRecordAPIView(APIView):

    def post(self, request):
        record_call_id = request.data.get('call_id', None)
        record_type = request.data.get('type', None)
        record_timestamp = request.data.get('timestamp', None)

        if not all([record_call_id, record_type, record_timestamp]):
            return Response(status=400)

        if record_type == PhoneRecord.TYPE_START:
            source = request.data.get('source', None)
            destination = request.data.get('destination', None)

            if not all([source, destination]):
                return Response(status=400)

        PhoneRecord.get_object_from_json(request.data)

        return Response(status=200)



class PhoneRecordBillsAPIView(APIView):

    def get(self, request, origin_number, reference_period=None):

        if reference_period:
            period_data = reference_period.split('-')

            if len(period_data) != 2:
                return Response(status=400)

            year, month = period_data
            reference_period = date(year=int(year), month=int(month), day=1)

        bills = PhoneRecord.get_period_bill(origin_number, reference_period)
        return Response(bills, status=200)
