
import math
from datetime import datetime
from decimal import Decimal
from datetime import timedelta


class PhoneRecordTariffCalculatorMixin(object):
    BASE_CALL_TARIFF = .36
    MINUTE_CHARGE = .09
    REDUCED_MINUTE_CHARGE = 0
    FREE_PERIOD_HOURS = (22, 23, 0, 1, 2, 3, 4, 5)

    @property
    def tariff(self):
        '''
        Method responsible for calculating the call chage by applying the specific call rules.
        '''
        duration = (self.record_end - self.record_start).total_seconds()

        if duration < 60:
            return self.BASE_CALL_TARIFF

        else:
            total_charge = int(duration/60) * self.MINUTE_CHARGE
            iter_date = self.record_start
            while iter_date < self.record_end:
                if iter_date.hour in self.FREE_PERIOD_HOURS:
                    total_charge -= (self.MINUTE_CHARGE - self.REDUCED_MINUTE_CHARGE)
                iter_date += timedelta(minutes=1)

            total_charge += self.BASE_CALL_TARIFF
            return float('%.2f' % total_charge)

    class Meta:
        abstract = True


class PhoneRecordPeriodBillMixin(object):

    @staticmethod
    def is_closed_period(reference_period):
        curr_date = datetime.today()

        if curr_date.year > reference_period.year:
            return True
        return True if reference_period.month < curr_date.month else False

    @staticmethod
    def get_last_closed_period():
        curr_date = datetime.today().replace(day=1)
        return (curr_date - timedelta(days=1)).replace(day=1)

    class Meta:
        abstract = True
